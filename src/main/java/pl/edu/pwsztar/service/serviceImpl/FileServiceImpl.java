package pl.edu.pwsztar.service.serviceImpl;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.core.io.InputStreamResource;
        import org.springframework.data.domain.Sort;
        import org.springframework.stereotype.Service;
        import pl.edu.pwsztar.domain.converter.Converter;
        import pl.edu.pwsztar.domain.dto.FileDto;
        import pl.edu.pwsztar.domain.dto.MovieDto;
        import pl.edu.pwsztar.domain.entity.Movie;
        import pl.edu.pwsztar.domain.files.FileGeneratorTxt;
        import pl.edu.pwsztar.domain.repository.MovieRepository;
        import pl.edu.pwsztar.service.FileService;

        import java.io.*;
        import java.util.List;

@Service
public class FileServiceImpl implements FileService, FileGeneratorTxt {

    private final MovieRepository movieRepository;
    private final Converter<List<Movie>, List<FileDto>> converter;

    @Autowired
    public FileServiceImpl(MovieRepository movieRepository, Converter<List<Movie>, List<FileDto>> converter) {
        this.movieRepository = movieRepository;
        this.converter = converter;
    }

    @Override
    public InputStreamResource toTxt(List<FileDto> fileDtoList) throws IOException {
        File file=File.createTempFile("tmp", ".txt");
        writeNewMovieList(fileDtoList, file);
        return getInputStream(file);
    }

    @Override
    public InputStreamResource getInputStream(File file) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(file);
        return new InputStreamResource(inputStream);
    }



    @Override
    public void writeNewMovieList(List<FileDto> fileDtoList, File file) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(file);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));

        fileDtoList.stream()
                .forEach(fileDto -> {
                    try {
                        bufferedWriter.write(fileDto.getYear() + " " + fileDto.getTitle());
                        bufferedWriter.newLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

        bufferedWriter.close();
        outputStream.flush();
        outputStream.close();
    }

    @Override
    public List<FileDto> getSortedMoviesList() {
        return converter.convert(movieRepository.findAll(Sort.by("year").descending()));
    }
}
