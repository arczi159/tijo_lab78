package pl.edu.pwsztar.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.files.FileGeneratorTxt;
import pl.edu.pwsztar.service.FileService;

import java.io.*;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value="/api")
public class FileApiController {

    private final FileService fileService;
    private final FileGeneratorTxt fileGeneratorTxt;
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    public FileApiController(FileService fileService, FileGeneratorTxt fileGeneratorTxt) {
        this.fileService = fileService;
        this.fileGeneratorTxt = fileGeneratorTxt;
    }

    @CrossOrigin
    @GetMapping(value = "/download-txt")
    public ResponseEntity<Resource> downloadTxt() throws IOException {
        LOGGER.info("--- download txt file ---");

        // TODO: --- Kod wymagajacy refaktoryzacji ---
        // TODO: Zanim zaczniesz refaktorowac pomysl o zasadzie KISS
        List<FileDto> fileDtoList = fileService.getSortedMoviesList();
        Resource resource = fileGeneratorTxt.toTxt(fileDtoList);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + "list_movies_"+(new Date().getTime())+".txt")
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);

        // TODO: --- Kod wymagajacy refaktoryzacji ---
    }
}
